import cv2
from config import *
from helper import *
import threading
import time
from huey_worker import sendAlerts, upload_nvr_status
import time
from concurrent.futures import ThreadPoolExecutor

#res = requests.get('https://backend.agrexai.com/api/allnvr/cloud-monitoring-enabled/')
#output =res.json()
output=[{
    "id": 834,
    "name": "Noida DVR",
    "brand": "Hikvision",
    "ip_address": "10.1.136.31",
    "channel_capacity": "16",
    "cameras_connected": 16,
    "channels_unused": [],
    "username": "spectra",
    "password": "Spectra@1010",
    "https_port_number": None,
    "http_port_number": 80,
    "rtsp_port": 554,
    "cloud_monitoring_enabled": False,
    "installed_at": 529,
    "Camera": 1999
  }]


with ThreadPoolExecutor(max_workers=len(output)) as executor:
    # Submit a call to the health function for each camera to the executor
    futures = [executor.submit(health, camera_ip, nvr_username, nvr_password, cam_list, nvr_id,camera_id, store_id) for nvr_dict in output for nvr_id, base_url, camera_ip, nvr_username, nvr_password, cam_list, camera_id, store_id in (make_url(nvr_dict),)]

    # Wait for all the futures to complete
    for future in futures:
        future.result()
